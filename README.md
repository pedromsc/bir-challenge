# BIR Challenge #


---

## PT-BR ##

### O que � o desafio? ###

O desafio consiste em programar um rob� para resolver um labirinto de forma aut�noma e encontrar uma esfera preta, que pode ser posicionada em qualquer lugar dentro do labirinto.


### Como tentei resolver o desafio? ###

* Primeiramente, selecionei um algoritmo de resolu��o de labirintos que melhor se adequa a situa��o. Escolhi, ent�o, o [algoritmo de Tr�maux](http://www.astrolog.org/labyrnth/algrithm.htm#solve), por que � um m�todo eficiente e n�o necessita conhecimento pr�vio do labirinto.
* Ent�o, implementei-o em LUA, com algumas modifica��es para melhor se adequar ao caso em quest�o.

### Limita��es atuais ###

* Devido ao passo de tempo impreciso do simulador, o rob� pode come�ar a girar quando tentar fazer uma curva. Pela mesma raz�o, as curvas podem ficar erradas por alguns graus.
* Devido ao input lag dos sensores, o rob� pode bater nas paredes.
* Devido a raz�es desconhecidas, o rob� pode fazer detec��es erradas e assumir rota��es imprevistas.


#### Caso algum desses casos ocorra, favor reiniciar a simula��o, ela deve se consertar.

### Poss�veis melhorias ###

* Adicionar um sistema de PID, para consertar os erros existentes nas curvas.


---

## EN-US ##

### What is the challenge? ###

The challenge is to program a robot to autonomously solve a maze and find a black sphere, which can be placed anywhere inside the maze.


### How did I tried to solve it? ###

* Firstly, I selected the solving algorithm that suited best this particular case, I chose the [Tr�maux's algorithm](http://www.astrolog.org/labyrnth/algrithm.htm#solve), because it's a efficient method and can be done without prior knowledge of the maze.
* Then, I implemented it in LUA, with some modifications to better fit the current case.

### Current limitations ###

* Due to the imprecise time steps of the simulator, the robot can start to spin when trying to turn. And for the same reason, turns can be a few degrees off track.
* Due to the input lag of the sensors, the robot can bump into walls.
* Due to unkown reasons, the robot can do false detections and do intended curves.

#### If any of those cases happens, please restart the simulation, it should fix itself.

### Possible improvements ###

* Adding a proper PID system, to fix the imprecise turns.

